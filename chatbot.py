from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
chatbot=ChatBot("Basic Bot")
trainer=ChatterBotCorpusTrainer(chatbot)
trainer.train("chatterbot.corpus.english")
print("Hello!I'm your chatbot. Type 'bye' to exit.")
while True:
    user_input=input("You : ")
    if user_input.lower()=='bye':
        print("chatbot: Goodbye!")
        break
    response=chatbot.get_response(user_input)
    print("Chatbot : ",response)
